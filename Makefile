export OMPI := /<path-to-my-openmpi-installation>/openmpi-install
export PATH := $(PATH):$(OMPI)/bin
export LD_LIBRARY_PATH := $(LD_LIBRARY_PATH):$(OMPI)/bin
# requirement: GNU make, see https://unix.stackexchange.com/questions/11530/adding-directory-to-path-through-makefile/261844#261844

CC = gcc
SRC = locserv hello_c
OBJ = $(SRC:.c=.o)
CFLAGS = -W -Wall -Wextra -Wpedantic -pedantic

.PHONY: clean

all: locserv hello

com_util.o: com_util.c
	$(CC) $(CFLAGS) -c $<

locserv: locserv.c com_util.o
	$(CC) $(CFLAGS) -o $@ $< com_util.o

hello: hello_c.c com_util.o
	mpicc -o $@ $< com_util.o

clean:
	rm -f $(OBJ) com_util.o
