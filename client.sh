#!/bin/bash

export OMPI=/<path-to-my-openmpi-installation>/openmpi-install
export PATH=$PATH:$OMPI/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OMPI/bin

PROCESSES=1

HOSTFILE="--hostfile hostfile.txt"
ENABLE_HYPERTHREADING="--bind-to hwthread"
ALLOW_OVERLOAD="--bind-to core:overload-allowed"
ENABLE_OVERSUBSCRIBE="--map-by :OVERSUBSCRIBE"
# --bind-to hwthread: makes the usage of threads instead of just cores possible
mpirun --np $PROCESSES $ALLOW_OVERLOAD $ENABLE_OVERSUBSCRIBE $HOSTFILE hello
